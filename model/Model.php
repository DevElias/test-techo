<?php

class Model
{    
    function GravarFabrica($aDados)
    {
        require_once 'Database.class.php';
        $sql = "";
        $sql .= "INSERT INTO fabrica (";
        $sql .= "fabrica_id, ";
        $sql .= "fabrica_nombre) VALUES (";
        $sql .= " NULL, ";
        $sql .= "'". $aDados['nombre'] ."')";
        $pdo = Database::conexao();
        $stmt = $pdo->prepare($sql); 
        $stmt->execute(); 
        $bGravou = $stmt->rowCount(); 

        if($bGravou == 1)
        {
            $Logado = true;
        }
        else
        {
            $Logado = false;
        }
         
        return $Logado;
    }
    
    function ListaFabrica()
    {
        require_once 'Database.class.php';
        $sql = "SELECT * FROM fabrica ORDER BY fabrica_nombre ASC";
        $pdo = Database::conexao();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $result;
    }
    
    function GravarModelo($aDados)
    {
        require_once 'Database.class.php';
        $sql = "";
        $sql .= "INSERT INTO modelo (";
        $sql .= "modelo_id, ";
        $sql .= "modelo_nombre, ";
        $sql .= "id_fabrica) VALUES (";
        $sql .= " NULL, ";
        $sql .= "'". $aDados['nombre'] ."', ";
        $sql .= "'". $aDados['fabrica'] ."')";
        $pdo = Database::conexao();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $bGravou = $stmt->rowCount();
        
        if($bGravou == 1)
        {
            $Logado = true;
        }
        else
        {
            $Logado = false;
        }
        
        return $Logado;
    }
    
    function ListaModelo()
    {
        require_once 'Database.class.php';
        $sql = "SELECT * FROM modelo ORDER BY modelo_nombre ASC";
        $pdo = Database::conexao();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $result;
    }
    
    function BuscaModelo($aDados)
    {
        require_once 'Database.class.php';
        $sql = "SELECT * FROM modelo WHERE id_fabrica = " . $aDados['idFabrica'];
        $pdo = Database::conexao();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $result;
    }
    
    function GravarVehiculo($aDados)
    {
        require_once 'Database.class.php';
        $sql = "";
        $sql .= "INSERT INTO veiculo (";
        $sql .= "veiculo_id, ";
        $sql .= "veiculo_color, ";
        $sql .= "veiculo_motor, ";
        $sql .= "veiculo_kilometraje, ";
        $sql .= "id_fabrica, ";
        $sql .= "id_modelo) VALUES (";
        $sql .= " NULL, ";
        $sql .= "'". $aDados['color'] ."', ";
        $sql .= "'". $aDados['motor'] ."', ";
        $sql .= "'". $aDados['kilometraje'] ."', ";
        $sql .= "'". $aDados['fabrica'] ."', ";
        $sql .= "'". $aDados['modelo'] ."')";
        $pdo = Database::conexao();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $bGravou = $stmt->rowCount();
        
        if($bGravou == 1)
        {
            $Logado = true;
        }
        else
        {
            $Logado = false;
        }
        
        return $Logado;
    }
    
    function ListagemFabrica()
    {
        require_once 'Database.class.php';
        $sql = "SELECT * FROM fabrica";
        $pdo = Database::conexao();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $result;
    }
    
    function ListagemModelo()
    {
        require_once 'Database.class.php';
        $sql = "SELECT * FROM modelo";
        $pdo = Database::conexao();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $result;
    }
    
    function ListagemVehiculo()
    {
        require_once 'Database.class.php';
        $sql = "SELECT veiculo_id, modelo_nombre, veiculo_motor, fabrica_nombre, veiculo_kilometraje, veiculo_color FROM veiculo INNER JOIN modelo ON modelo.modelo_id = veiculo.id_modelo INNER JOIN fabrica ON fabrica_id = veiculo.id_fabrica";
        $pdo = Database::conexao();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $result;
    }
    
    function ExcluirFabrica($id)
    {
        require_once 'Database.class.php';
        $sql = "DELETE FROM fabrica WHERE fabrica_id = " . $id;
        $pdo = Database::conexao();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $bExcluiu = $stmt->rowCount();
        
        if($bExcluiu == 1)
        {
            $Retorno = true;
        }
        else
        {
            $Retorno = false;
        }
        
        return $Retorno;
    }
    
    function ExcluirModelo($id)
    {
        require_once 'Database.class.php';
        $sql = "DELETE FROM modelo WHERE modelo_id = " . $id;
        $pdo = Database::conexao();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $bExcluiu = $stmt->rowCount();
        
        if($bExcluiu == 1)
        {
            $Retorno = true;
        }
        else
        {
            $Retorno = false;
        }
        
        return $Retorno;
    }
    
    function ExcluirVeiculo($id)
    {
        require_once 'Database.class.php';
        $sql = "DELETE FROM veiculo WHERE veiculo_id = " . $id;
        $pdo = Database::conexao();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $bExcluiu = $stmt->rowCount();
        
        if($bExcluiu == 1)
        {
            $Retorno = true;
        }
        else
        {
            $Retorno = false;
        }
        
        return $Retorno;
    }
    
    function LocalizaFabrica($id)
    {
        require_once 'Database.class.php';
        $sql = "SELECT * FROM fabrica WHERE fabrica_id = " . $id;
        $pdo = Database::conexao();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $result;
    }
    
    function LocalizaModelo($id)
    {
        require_once 'Database.class.php';
        $sql = "SELECT * FROM modelo WHERE modelo_id = " . $id;
        $pdo = Database::conexao();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $result;
    }
    
    function BuscaVehiculo($id)
    {
        require_once 'Database.class.php';
        $sql = "SELECT * FROM veiculo WHERE veiculo_id = " . $id;
        $pdo = Database::conexao();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $result;
    }
    
    function BuscaModelo2($idFabrica)
    {
        require_once 'Database.class.php';
        $sql = "SELECT * FROM modelo WHERE id_fabrica =" . $idFabrica;
        $pdo = Database::conexao();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $result;
    }
    
    //Altera Fabrica
    function AtualizaFabrica($aDados)
    {
        require_once 'Database.class.php';
        $sql = "";
        $sql .= "UPDATE fabrica SET ";
        $sql .= "fabrica_nombre = '" .$aDados['nombre'] ."'";
        $sql .= "WHERE fabrica_id = " . $aDados['id'];
        $pdo = Database::conexao();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $bGravou = $stmt->rowCount();
        
        if($bGravou == 1)
        {
            $Logado = true;
        }
        else
        {
            $Logado = false;
        }
        
        return $Logado;
    }
    
    //Altera Fabrica
    function AtualizaModelo($aDados)
    {
        require_once 'Database.class.php';
        $sql = "";
        $sql .= "UPDATE modelo SET ";
        $sql .= "modelo_nombre = '" .$aDados['nombre'] ."', ";
        $sql .= "id_fabrica = '" .$aDados['fabrica'] ."'";
        $sql .= "WHERE modelo_id = " . $aDados['id'];
        $pdo = Database::conexao();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $bGravou = $stmt->rowCount();
        
        if($bGravou == 1)
        {
            $Logado = true;
        }
        else
        {
            $Logado = false;
        }
        
        return $Logado;
    }
    
    //Altera Vehiculo
    function AtualizaVehiculo($aDados)
    {
        require_once 'Database.class.php';
        $sql = "";
        $sql .= "UPDATE veiculo SET ";
        $sql .= "veiculo_color = '" .$aDados['color'] ."', ";
        $sql .= "veiculo_motor = '" .$aDados['motor'] ."', ";
        $sql .= "veiculo_kilometraje = " .$aDados['kilometraje'] . ", ";
        $sql .= "id_fabrica = " .$aDados['fabrica'] .", ";
        $sql .= "id_modelo = " .$aDados['modelo'] ." ";
        $sql .= "WHERE veiculo_id = " . $aDados['id'];
        $pdo = Database::conexao();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $bGravou = $stmt->rowCount();
        
        if($bGravou == 1)
        {
            $Logado = true;
        }
        else
        {
            $Logado = false;
        }
        
        return $Logado;
    }
    
}
?>