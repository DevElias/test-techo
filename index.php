<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Home</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="js/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="css/AdminLTE.min.css">
  <link rel="stylesheet" href="css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="css/custom.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <?php include 'view/partial/header.php'; ?>
  <div class="content-wrapper">
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="http://localhost/techo/index.php"><i class="fa fa-home"></i> Home</a></li>
      </ol>
    </section>
    
    <section class="content">
        <a class="btn btn-app bg-blue" href="http://localhost/techo/view/incluir-fabrica.php">
        <i class="fa fa-building"></i>A&ntilde;adir Fabrica
      </a>

      <a class="btn btn-app bg-blue" href="http://localhost/techo/view/incluir-modelo.php">
        <i class="fa fa-bicycle"></i> A&ntilde;adir Modelo
      </a>
      
       <a class="btn btn-app bg-blue" href="http://localhost/techo/view/incluir-vehiculo.php">
        <i class="fa fa-car"></i> A&ntilde;adir veh&iacute;culos
      </a>
	  
	  </br>
	  
      <a class="btn btn-app bg-green" href="view/listado-fabrica.php">
        <i class="fa fa-file-text"></i> Listado de Fabrica
      </a>


      <a class="btn btn-app bg-green" href="view/listado-modelo.php">
        <i class="fa fa-file-text"></i> Listado de Modelo
      </a>

      <a class="btn btn-app bg-green" href="view/listado-vehiculo.php">
        <i class="fa fa-file-text"></i> Listado de veh&iacute;culos
      </a>

    </section>
  </div>
  <div class="control-sidebar-bg"></div>
</div>

<script src="js/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="js/bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="js/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="js/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="js/app.min.js"></script>
</body>
</html>
