$('#loading').hide(); //Sempre esconde
//Metodo de Gravacao de Modelo
function GravarModelo()
{	
	oData          = new Object();	
	oData.acao     = "GravarModelo";
	oData.nombre   = $('#nombre').val();
	oData.fabrica  = $('#fabrica').val();
		
	$('#loading').show();
	
	$.ajax({
				type: "POST",
				url: "../controller/Controller.php",
				dataType: "json",
				data: oData,
				success: function(oData)
				{	
					if(oData['results'])
					{
						$.confirm({
						    content: "Grabado con Exito.",
						    buttons: {
						        ok: function(){
						            location.href = "http://localhost/techo/index.php";
						        }
						    }
						});
						
						$('#loading').hide();
					}
					else
					{
						$.confirm({
						    content: "Error al grabar.",
						    buttons: {
						        ok: function(){
						            location.href = "http://localhost/techo/view/incluir-modelo.php";
						        }
						    }
						});
						
						$('#loading').hide();
					}
				}
			});	
}

//Metodo de Exclusão de Modelos
function DeleteModelo(id)
{
	oData          = new Object();	
	oData.acao     = "ExcluirModelo";
	oData.id       = id;
	
	$.ajax({
		type: "POST",
		url: "../controller/Controller.php",
		dataType: "json",
		data: oData,
		success: function(oData)
		{
			if(oData['results'])
			{
				$.confirm({
				    content: "Excluido com Sucesso.",
				    buttons: {
				        ok: function(){
				            location.href = "http://localhost/techo/view/listado-modelo.php";
				        }
				    }
				});
				
				//location.reload();
			}
			else
			{
				$.confirm({
				    content: "Erro ao Excluir.",
				    buttons: {
				        ok: function(){
				            location.href = "http://localhost/techo/view/listado-modelo.php";
				        }
				    }
				});
			}
		}
	});	
}

//Metodo para Carregar Dados dos Modelos (Edição)
function EditModelo(id)
{
	window.location.assign("http://localhost/techo/view/editar-modelo.php/?id="+ id);
}

//Metodo de Alterar os Modelos
function AtualizaModelo(id)
{
	oData          = new Object();	
	oData.acao     = "AtualizaModelo";
	oData.id       = id;
	oData.nombre   = $('#nombre').val(); 
	oData.fabrica  = $('#fabrica').val();
	
	$('#loading').show();
	
	$.ajax({
		type: "POST",
		url: "http://localhost/techo/controller/Controller.php",
		dataType: "json",
		data: oData,
		success: function(oData)
		{
			if(oData['results'])
			{
				$.confirm({
				    content: "Alterado com Sucesso.",
				    buttons: {
				        ok: function(){
				            location.href = "http://localhost/techo/view/listado-modelo.php";
				        }
				    }
				});
				
				$('#loading').hide();
			}
			else
			{
				$.confirm({
				    content: "Erro ao Editar.",
				    buttons: {
				        ok: function(){
				            location.href = "http://localhost/techo/home.php";
				        }
				    }
				});
				
				$('#loading').hide();
			}
		}
	});	
}
