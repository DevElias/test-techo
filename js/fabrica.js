$('#loading').hide(); //Sempre esconde
//Metodo de Gravacao de Fabrica
function GravarFabrica()
{	
	oData          = new Object();	
	oData.acao     = "GravarFabrica";
	oData.nombre   = $('#nombre').val(); 
		
	$('#loading').show();
	
	$.ajax({
				type: "POST",
				url: "../controller/Controller.php",
				dataType: "json",
				data: oData,
				success: function(oData)
				{	
					if(oData['results'])
					{
						$.confirm({
						    content: "Grabado con Exito.",
						    buttons: {
						        ok: function(){
						            location.href = "http://localhost/techo/index.php";
						        }
						    }
						});
						
						$('#loading').hide();
					}
					else
					{
						$.confirm({
						    content: "Error al grabar.",
						    buttons: {
						        ok: function(){
						            location.href = "http://localhost/techo/view/incluir-fabrica.php";
						        }
						    }
						});
						
						$('#loading').hide();
					}
				}
			});	
}

//Metodo de Exclusão de Fabricas
function DeleteFabrica(id)
{
	oData          = new Object();	
	oData.acao     = "ExcluirFabrica";
	oData.id       = id;
	
	$.ajax({
		type: "POST",
		url: "../controller/Controller.php",
		dataType: "json",
		data: oData,
		success: function(oData)
		{
			if(oData['results'])
			{
				$.confirm({
				    content: "Excluido com Sucesso.",
				    buttons: {
				        ok: function(){
				            location.href = "http://localhost/techo/view/listado-fabrica.php";
				        }
				    }
				});
				
				//location.reload();
			}
			else
			{
				$.confirm({
				    content: "Erro ao Excluir.",
				    buttons: {
				        ok: function(){
				            location.href = "http://localhost/techo/view/listado-fabrica.php";
				        }
				    }
				});
			}
		}
	});	
}

//Metodo para Carregar Dados das Fabricas (Edição)
function EditFabfrica(id)
{
	window.location.assign("http://localhost/techo/view/editar-fabrica.php/?id="+ id)
}

//Metodo de Alterar as Fabricas
function AtualizaFabrica(id)
{
	oData          = new Object();	
	oData.acao     = "AtualizaFabrica";
	oData.id       = id;
	oData.nombre   = $('#nombre').val(); 
	
	$('#loading').show();
	
	$.ajax({
		type: "POST",
		url: "http://localhost/techo/controller/Controller.php",
		dataType: "json",
		data: oData,
		success: function(oData)
		{
			if(oData['results'])
			{
				$.confirm({
				    content: "Alterado com Sucesso.",
				    buttons: {
				        ok: function(){
				            location.href = "http://localhost/techo/view/listado-fabrica.php";
				        }
				    }
				});
				
				$('#loading').hide();
			}
			else
			{
				$.confirm({
				    content: "Erro ao Editar.",
				    buttons: {
				        ok: function(){
				            location.href = "http://localhost/techo/home.php";
				        }
				    }
				});
				
				$('#loading').hide();
			}
		}
	});	
}
