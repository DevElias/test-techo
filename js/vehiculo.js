$('#loading').hide(); //Sempre esconde

//Metodo que Busca o Modelo
function BuscaModelo()
{	
	oData           = new Object();	
	oData.acao      = "BuscaModelo";
	oData.idFabrica = $('#fabrica').val();
	
	$('#loading').show();
	
	$.ajax({
				type: "POST",
				url: "../controller/Controller.php",
				dataType: "json",
				data: oData,
				success: function(oData)
				{	
					if(oData['results'])
					{
												
						$('#loading').hide();
						$('#cModelo').html(oData['results']);
					}
					else
					{
						$('#loading').hide();
					}
				}
			});	
}

function BuscaModelo2()
{	
	oData           = new Object();	
	oData.acao      = "BuscaModelo";
	oData.idFabrica = $('#fabrica').val();
	
	$('#loading').show();
	
	$.ajax({
				type: "POST",
				url: "http://localhost/techo/controller/Controller.php",
				dataType: "json",
				data: oData,
				success: function(oData)
				{	
					if(oData['results'])
					{
												
						$('#loading').hide();
						$('#cModelo').html(oData['results']);
					}
					else
					{
						$('#loading').hide();
					}
				}
			});	
}

//Metodo que Grava os Vehiculos
function GravarVehiculo()
{	
	oData             = new Object();	
	oData.acao        = "GravarVehiculo";
	oData.color       = $('#color').val();
	oData.kilometraje = $('#kilometraje').val();
	oData.motor       = $('#motor').val();
	oData.fabrica     = $('#fabrica').val();
	oData.modelo      = $('#modelo').val();
		
	$('#loading').show();
	
	$.ajax({
				type: "POST",
				url: "../controller/Controller.php",
				dataType: "json",
				data: oData,
				success: function(oData)
				{	
					if(oData['results'])
					{
						$.confirm({
						    content: "Grabado con Exito.",
						    buttons: {
						        ok: function(){
						            location.href = "http://localhost/techo/index.php";
						        }
						    }
						});						
						$('#loading').hide();
					}
					else
					{
						$.confirm({
						    content: "Error al grabar.",
						    buttons: {
						        ok: function(){
						            location.href = "http://localhost/techo/view/incluir-fabrica.php";
						        }
						    }
						});
						$('#loading').hide();
					}
				}
			});	
}

//Metodo de Exclusão de Vehiculos
function DeleteVehiculo(id)
{
	oData          = new Object();	
	oData.acao     = "ExcluirVeiculo";
	oData.id       = id;
	
	$.ajax({
		type: "POST",
		url: "../controller/Controller.php",
		dataType: "json",
		data: oData,
		success: function(oData)
		{
			if(oData['results'])
			{
				$.confirm({
				    content: "Excluido com Sucesso.",
				    buttons: {
				        ok: function(){
				            location.href = "http://localhost/techo/view/listado-vehiculo.php";
				        }
				    }
				});
				
				//location.reload();
			}
			else
			{
				$.confirm({
				    content: "Erro ao Excluir.",
				    buttons: {
				        ok: function(){
				            location.href = "http://localhost/techo/view/listado-vehiculo.php";
				        }
				    }
				});
			}
		}
	});	
}

//Metodo para Carregar Dados dos Vehiculos (Edição)
function EditVehiculo(id)
{
	window.location.assign("http://localhost/techo/view/editar-vehiculo.php/?id="+ id);
}

//Metodo de Alterar os Vehiculos
function AtualizaVehiculo(id)
{
	oData          = new Object();	
	oData.acao     = "AtualizaVehiculo";
	oData.id       = id;
	oData.modelo   = $('#modelo').val();
	oData.fabrica  = $('#fabrica').val();
	oData.kilometraje  = $('#kilometraje').val();
	oData.color    = $('#color').val();
	oData.motor    = $('#motor').val();
	
	$('#loading').show();
	
	$.ajax({
		type: "POST",
		url: "http://localhost/techo/controller/Controller.php",
		dataType: "json",
		data: oData,
		success: function(oData)
		{
			if(oData['results'])
			{
				$.confirm({
				    content: "Alterado com Sucesso.",
				    buttons: {
				        ok: function(){
				            location.href = "http://localhost/techo/view/listado-vehiculo.php";
				        }
				    }
				});
				
				$('#loading').hide();
			}
			else
			{
				$.confirm({
				    content: "Erro ao Editar.",
				    buttons: {
				        ok: function(){
				            location.href = "http://localhost/techo/home.php";
				        }
				    }
				});
				
				$('#loading').hide();
			}
		}
	});	
}
