<?php
//Busca Lista de Fabricas
require_once '../model/Model.php';
$oBj = new Model();
$aDados = $oBj->ListaFabrica();

$comboBoxFabrica = '';
$comboBoxFabrica .= '<select name="fabrica" id="fabrica" class="form-control" >';
$comboBoxFabrica .= '<option value="0">-- SELECIONE --</option>';
foreach ($aDados as $k => $v)
{
    $comboBoxFabrica .= '<option value="'.$v['fabrica_id'] .'">'. $v['fabrica_nombre'] .'</option>';
}
$comboBoxFabrica .= '</select>';

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Modelo</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../js/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="../css/AdminLTE.min.css">
  <link rel="stylesheet" href="../css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="../css/custom.css">
  <link rel="stylesheet" type="text/css" href="../css/jquery-confirm.css"/>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <?php include 'partial/header.php'; ?>
  <div class="content-wrapper">
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">A&ntilde;adir Modelo</h3>
              </div>
              <form role="form">
                <div class="box-body">
                  <div class="form-group">
                    <label for="modelo">Nombre</label>
                    <input type="text" name="nombre" id="nombre" placeholder="Ex: Fusion" class="form-control"/>
                  </div>
                   <div class="form-group">
                    <label for="modelo">Fabrica</label>
                    <?php echo($comboBoxFabrica);?>
                  </div>
                </div>
                <div class="box-footer">
                  <button id="btn-grava" type="button" onclick="GravarModelo();" class="btn btn-block btn-primary">Gravar</button>
                </div>
              </form>
            </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <div id="loading"><img src="http://www.macintel.com.br/app/img/loading.gif"></div> 
  <div class="control-sidebar-bg"></div>
</div>
<script type="text/javascript">

<script type="text/javascript">
<script src="../js/jquery.min.js"></script>
<script src="../js/jQuery/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../js/jquery-confirm.js"></script>

<!-- Bootstrap 3.3.6 -->
<script src="../js/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/main.js"></script>
<script type="text/javascript" src="../js/modelo.js"></script>
<!-- SlimScroll -->
<script src="../js/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../js/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../js/app.min.js"></script>
</body>
</html>
