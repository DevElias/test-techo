<?php
//Busca os Modelos
require_once '../model/Model.php';
$oBj = new Model();
$aRet = $oBj->ListagemVehiculo();

$html = '';
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Listado de Vehiculos</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../js/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="../css/AdminLTE.min.css">
  <link rel="stylesheet" href="../css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="../css/custom.css">
  <link rel="stylesheet" type="text/css" href="../css/jquery-confirm.css"/>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <?php include 'partial/header.php'; ?>
  <div class="content-wrapper">
    <section class="content-header">
      <ol class="breadcrumb">
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Listado de Vehiculos</h3>
            </div>
            <div class="box-body">
              <table id="listagem" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th></th>
                  <th>Modelo</th>
                  <th>Fabrica</th>
                  <th>Kilometrajem</th>
                  <th>color</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
               	<?php 
                       	$Contador = 1;
                       	for($i=0; $i < count($aRet); $i++)
                       	{ 
                       	    $html .= '<tr>';
                       	    $html .= '<td>' . $Contador . '</td>';
                       	    $html .= '<td>' . $aRet[$i]['modelo_nombre'] . '</td>';
                       	    $html .= '<td>' . $aRet[$i]['fabrica_nombre'] . '</td>';
                       	    $html .= '<td>' . $aRet[$i]['veiculo_kilometraje'] . '</td>';
                       	    $html .= '<td>' . $aRet[$i]['modelo_color'] . '</td>';
                       	    $html .= '<td><a type="button" onclick="EditVehiculo('. $aRet[$i]['veiculo_id'] .');" class="btn btn-warning"><i class="fa fa-edit"></i></a>';
                       	    $html .= '<a type="button" onclick="DeleteVehiculo('. $aRet[$i]['veiculo_id'] .');" class="btn btn-danger"> <i class="fa  fa-trash"></i> </a></td>';
                       	    $html .= '</tr>';
                       	    
                       	    $Contador++;
                       	}
                       	echo($html);
               	?>       
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div class="control-sidebar-bg"></div>
</div>
<!-- jQuery 2.2.3 -->
<script src="../js/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../js/bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="http://www.macintel.com.br/app/js/datatables/jquery.dataTables.min.js"></script>
<script src="http://www.macintel.com.br/app/js/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../js/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../js/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../js/app.min.js"></script>

<script type="text/javascript" src="http://www.macintel.com.br/app/js/main.js"></script>
<script type="text/javascript" src="http://www.macintel.com.br/app/js/jquery-confirm.js"></script>

<script>
  $(function () {
    $('#listagem').DataTable({
      "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
            }
    });
  });
</script>
</body>
</html>
