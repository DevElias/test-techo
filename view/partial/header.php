﻿  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="header">MENÚ NAVEGACIÓN</li>
        <li class="treeview">
          <a href="http://localhost/techo/index.php">
            <i class="fa fa-home"></i> <span>Home</span>
          </a>
        </li>
     
        <li class="treeview">
          <a href="#">
            <i class="fa fa-group"></i>
            <span>Catastros</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="http://localhost/techo/view/incluir-fabrica.php"><i class="fa fa-building"></i> Fabrica </a></li>
            <li><a href="http://localhost/techo/view/incluir-modelo.php"><i class="fa fa-bicycle"></i> Modelo</a></li>
            <li><a href="http://localhost/techo/view/incluir-vehiculo.php"><i class="fa fa-car"></i> vehículos</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i>
            <span>Listado</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
           <li><a href="http://localhost/techo/view/listado-fabrica.php"><i class="fa fa-file-o"></i> Fabrica</a></li>
            <li><a href="http://localhost/techo/view/listado-modelo.php"><i class="fa fa-file-o"></i> Modelo</a></li>
            <li><a href="http://localhost/techo/view/listado-vehiculo.php"><i class="fa fa-file-o"></i> vehículos</a></li>
          </ul>
        </li>
      </ul>
    </section>
  </aside>
