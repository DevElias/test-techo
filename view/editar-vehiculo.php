<?php
//Busca Lista de Fabricas
require_once '../model/Model.php';
$oBj        = new Model();
$aDados     = $oBj->ListaFabrica();
$aRet       = $oBj->ListaModelo();

$id = $_GET['id'];
$aVehiculos = $oBj->BuscaVehiculo($id);

//ComboBox de Color
$comboBoxColor = '';
$comboBoxColor .= '<select name="color" id="color" class="form-control" >';
$comboBoxColor .= '<option value="0">-- SELECIONE -- </option>';

foreach ($aVehiculos as $k => $v)
{
    if($v['veiculo_color'] == 'Azul')
    {
        $comboBoxColor .= '<option selected value="Azul">Azul</option>';
    }
    else 
    {
        $comboBoxColor .= '<option value="Azul">Azul</option>';
    }
    
    if($v['veiculo_color'] == 'Verde')
    {
        $comboBoxColor .= '<option selected value="Verde">Verde</option>';
    }
    else 
    {
        $comboBoxColor .= '<option value="Verde">Verde</option>';
    }
    
    if($v['veiculo_color'] == 'Rojo')
    {
        $comboBoxColor .= '<option selected value="Rojo">Rojo</option>';
    }
    else
    {
        $comboBoxColor .= '<option value="Rojo">Rojo</option>';
    }
    
    if($v['veiculo_color'] == 'Prata')
    {
        $comboBoxColor .= '<option selected value="Prata">Prata</option>';
    }
    else 
    {
        $comboBoxColor .= '<option value="Prata">Prata</option>';
    }
    
    if($v['veiculo_color'] == 'Preto')
    {
        $comboBoxColor .= '<option selected value="Preto">Preto</option>';
    }
    else 
    {
        $comboBoxColor .= '<option value="Preto">Preto</option>';
    }
}
$comboBoxColor .= '</select>';
//Fim ComboBox Color

//ComboBox de ComboBoxKilometraje
$comboBoxKilometraje = '';
$comboBoxKilometraje .= '<select name="kilometraje" id="kilometraje" class="form-control" >';
$comboBoxKilometraje .= '<option value="">-- SELECIONE -- </option>';

foreach ($aVehiculos as $k => $v)
{
    if($v['veiculo_kilometraje'] == '0')
    {
        $comboBoxKilometraje .= '<option  selected value="0">0 Km</option>';
    }
    else
    {
        $comboBoxKilometraje .= '<option value="0">0 Km</option>';
    }
    
    if($v['veiculo_kilometraje'] == '5000')
    {
        $comboBoxKilometraje .= '<option selected value="5000">5000 Km</option>';
    }
    else
    {
        $comboBoxKilometraje .= '<option value="5000">5000 Km</option>';
    }
    
    if($v['veiculo_kilometraje'] == '1200')
    {
        $comboBoxKilometraje .= '<option selected value="1200">1200 Km</option>';
    }
    else
    {
        $comboBoxKilometraje .= '<option value="1200">1200 Km</option>';
    }
    
    if($v['veiculo_kilometraje'] == '1500')
    {
        $comboBoxKilometraje .= '<option selected value="1500">1500 Km</option>';
    }
    else 
    {
        $comboBoxKilometraje .= '<option value="1500">1500 Km</option>';
    }
    
    if($v['veiculo_kilometraje'] == '1800')
    {
        $comboBoxKilometraje .= '<option selected value="1800">1800 Km</option>';
    }
    else
    {
        $comboBoxKilometraje .= '<option value="1800">1800 Km</option>';
    }
}
$comboBoxKilometraje .= '</select>';
//Fim ComboBoxKilometraje

//ComboBox de ComboBoxMotor
$comboBoxMotor = '';
$comboBoxMotor .= '<select name="motor" id="motor" class="form-control" >';
$comboBoxMotor .= '<option value="0">-- SELECIONE -- </option>';

foreach ($aVehiculos as $k => $v)
{
    if($v['veiculo_motor'] == '1.0')
    {
        $comboBoxMotor .= '<option  selected value="1.0">1.0 Km</option>';
    }
    else
    {
        $comboBoxMotor .= '<option value="1.0">1.0</option>';
    }
    
    if($v['veiculo_motor'] == '1.4')
    {
        $comboBoxMotor .= '<option selected value="1.4">1.4</option>';
    }
    else
    {
        $comboBoxMotor .= '<option value="1.4">1.4</option>';
    }
    
    if($v['veiculo_motor'] == '1.8')
    {
        $comboBoxMotor .= '<option selected value="1.8">1.8</option>';
    }
    else
    {
        $comboBoxMotor .= '<option value="1.8">1.8</option>';
    }
    
    if($v['veiculo_motor'] == '2.0')
    {
        $comboBoxMotor .= '<option selected value="2.0">2.0</option>';
    }
    else
    {
        $comboBoxMotor .= '<option value="2.0">2.0</option>';
    }
}
$comboBoxMotor .= '</select>';
//Fim ComboBoxMotor

//ComboBox de Fabricas
$comboBoxFabrica = '';
$comboBoxFabrica .= '<select name="fabrica" id="fabrica" class="form-control" >';
$comboBoxFabrica .= '<option value="0">-- SELECIONE --</option>';
foreach ($aDados as $k => $v)
{
    if($v['fabrica_id'] == $aVehiculos[0]['id_fabrica'])
    {
        $comboBoxFabrica .= '<option selected value="'.$v['fabrica_id'] .'">'. $v['fabrica_nombre'] .'</option>';
    }
    else
    {
        $comboBoxFabrica .= '<option value="'.$v['fabrica_id'] .'">'. $v['fabrica_nombre'] .'</option>';
    }

}
$comboBoxFabrica .= '</select>';

//ComboBox de Modelos
$aModelos = $oBj->BuscaModelo2($aVehiculos[0]['id_fabrica']);

$comboBoxModelo = '';
$comboBoxModelo .= '<select name="modelo" id="modelo" class="form-control" >';
$comboBoxModelo .= '<option value="0">-- SELECIONE --</option>';
foreach ($aModelos as $k => $v)
{
    if($v['modelo_id'] == $aVehiculos[0]['id_modelo'])
    {
        $comboBoxModelo .= '<option selected value="'.$v['modelo_id'] .'">'. $v['modelo_nombre'] .'</option>';
    }
    else 
    {
        $comboBoxModelo .= '<option value="'.$v['modelo_id'] .'">'. $v['modelo_nombre'] .'</option>';
    }
}
$comboBoxModelo .= '</select>';

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Vehiculo</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="http://localhost/techo/js/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="http://localhost/techo/css/AdminLTE.min.css">
  <link rel="stylesheet" href="http://localhost/techo/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="http://localhost/techo/css/custom.css">
  <link rel="stylesheet" type="text/css" href="http://localhost/techo/css/jquery-confirm.css"/>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <?php include 'partial/header.php'; ?>
  <div class="content-wrapper">
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">A&ntilde;adir Vehiculo</h3>
              </div>
              <form role="form">
                <div class="box-body">
                 <div class="form-group">
                    <label for="vehiculo">Color</label>
                    <?php echo($comboBoxColor);?>
                  </div>
                  <div class="form-group">
                    <label for="vehiculo">Kilometraje</label>
                        <?php echo($comboBoxKilometraje)?>
                  </div>
                  <div class="form-group">
                    <label for="vehiculo">Motor</label>
                        <?php echo($comboBoxMotor);?>
                  </div>
                   <div class="form-group">
                    <label for="vehiculo">Fabrica</label>
                    <?php echo($comboBoxFabrica);?>
                  </div>
                  <div class="form-group" id="bModelos">
                    <label for="vehiculo">Modelo</label>
                    <?php echo($comboBoxModelo);?>
                  </div>
                   <label id="modelotext" for="vehiculo" hidden>Modelo</label>
                  <div id="cModelo">
                  </div>
                </div>
                <div class="box-footer">
                  <button id="btn-grava" type="button" onclick="AtualizaVehiculo(<?php echo($id);?>);" class="btn btn-block btn-primary">Gravar</button>
                </div>
              </form>
            </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <div class="control-sidebar-bg"></div>
</div>
<script type="text/javascript">
</script>
<script type="text/javascript">
</script>
<script src="http://localhost/techo/js/jquery.min.js"></script>
<script src="http://localhost/techo/js/jQuery/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="http://localhost/techo/js/jquery-confirm.js"></script>

<!-- Bootstrap 3.3.6 -->
<script src="http://localhost/techo/js/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://localhost/techo/js/main.js"></script>
<script type="text/javascript" src="http://localhost/techo/js/vehiculo.js"></script>
<!-- SlimScroll -->
<script src="http://localhost/techo/js/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="http://localhost/techo/js/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="http://localhost/techo/js/app.min.js"></script>
<script>
$("#fabrica").change(function(){
	$('#bModelos').hide();
	BuscaModelo2();
	$('#modelotext').show();
});
</script>
</body>
</html>
