<?php
//Busca Lista de Fabricas
require_once '../model/Model.php';
$oBj = new Model();
$aDados = $oBj->ListaFabrica();
$aRet   = $oBj->ListaModelo();

//ComboBox de Fabricas
$comboBoxFabrica = '';
$comboBoxFabrica .= '<select name="fabrica" id="fabrica" class="form-control" >';
$comboBoxFabrica .= '<option value="0">-- SELECIONE --</option>';
foreach ($aDados as $k => $v)
{
    $comboBoxFabrica .= '<option value="'.$v['fabrica_id'] .'">'. $v['fabrica_nombre'] .'</option>';
}
$comboBoxFabrica .= '</select>';

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Vehiculo</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../js/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="../css/AdminLTE.min.css">
  <link rel="stylesheet" href="../css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="../css/custom.css">
  <link rel="stylesheet" type="text/css" href="../css/jquery-confirm.css"/>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <?php include 'partial/header.php'; ?>
  <div class="content-wrapper">
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">A&ntilde;adir Vehiculo</h3>
              </div>
              <form role="form">
                <div class="box-body">
                  <div class="form-group">
                    <label for="vehiculo">Color</label>
                        <select name="color" id="color" class="form-control" >
                          <option value="0">-- SELECIONE -- </option>
                          <option value="Azul">Azul</option>
                          <option value="Verde">Verde</option>
                          <option value="Rojo">Rojo</option>
                          <option value="Prata">Prata</option>
                          <option value="Preto">Preto</option>
                        </select>
                  </div>
                  <div class="form-group">
                    <label for="vehiculo">Kilometraje</label>
                        <select name="kilometraje" id="kilometraje" class="form-control" >
                          <option value="0">-- SELECIONE -- </option>
                          <option value="0">0 Km</option>
                          <option value="5000">5000 Km</option>
                          <option value="1200">1200 Km</option>
                          <option value="1500">1500 Km</option>
                          <option value="1800">1800 Km</option>
                        </select>
                  </div>
                  <div class="form-group">
                    <label for="vehiculo">Motor</label>
                        <select name="motor" id="motor" class="form-control" >
                          <option value="">-- SELECIONE -- </option>
                          <option value="1.0">1.0</option>
                          <option value="1.4">1.4</option>
                          <option value="1.8">1.8</option>
                          <option value="2.0">2.0</option>
                        </select>
                  </div>
                   <div class="form-group">
                    <label for="vehiculo">Fabrica</label>
                    <?php echo($comboBoxFabrica);?>
                  </div>
                  <label for="vehiculo">Modelo</label>
                  <div id="cModelo"></div>
                </div>
                <div class="box-footer">
                  <button id="btn-grava" type="button" onclick="GravarVehiculo();" class="btn btn-block btn-primary">Gravar</button>
                </div>
              </form>
            </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <div id="loading"><img src="http://www.macintel.com.br/app/img/loading.gif"></div> 
  <div class="control-sidebar-bg"></div>
</div>
<script type="text/javascript">
</script>
<script type="text/javascript">
</script>
<script src="../js/jquery.min.js"></script>
<script src="../js/jQuery/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="../js/jquery-confirm.js"></script>

<!-- Bootstrap 3.3.6 -->
<script src="../js/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/main.js"></script>
<script type="text/javascript" src="../js/vehiculo.js"></script>
<!-- SlimScroll -->
<script src="../js/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../js/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../js/app.min.js"></script>
<script>
$("#fabrica").change(function(){
	BuscaModelo();
});
</script>
</body>
</html>
