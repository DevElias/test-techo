// PORTUGUêS //
Teste de Desenvolvedor na Web
TECHO

Este sistema tem como finalidade a análise do código fonte das funcionalidades para o processo seletivo.
A Aplicação é um CRUD de Automóveis, tendo cadastros de Fábrica, Modelos e Veículo. Baseado na Linguagem PHP/Jquery com Banco de Dados em MySQL e interface em HTML. 
A Principais funcionalidades são:

- Inclusão de Fábricas (Indústria automobilística).
- Edição de Fábrica.
- Exclusão de Fábrica.

- Inclusão de Modelos.
- Edição de Modelos.
- Exclução de Modelos.

- Inclusão de Veículos.
- Edição de Veículos.
- Exclusão de Veículos.

- Sistema de Filtro em cada tela.
 
 
 Instruções de como iniciar a aplicação
 
 1. Extrair o Arquivo Techo.rar em seu diretório htdocs ou fazer check-out do projeto através do git.
 2. O Projeto poderá ser acesso através do diretório http://localhost/techo
 3. Execute os Scripts de Banco que se encontram na pasta "htdocs\techo\scripts", se executados na sequencia númerica não terás problemas.
 4. Até o 4 Script são criações de tabelas, os demais são inserções de dados nas tabelas.
 
 
 // Español //
Prueba de deshacer del desarrollador en la Web
TECHO

Este sistema tiene como finalidad el análisis del código fuente de las funcionalidades para el proceso selectivo.
La aplicación es un CRUD de Automóviles, teniendo registros de Fábrica, Modelos y Vehículo. Basado en el lenguaje PHP / Jquery con base de datos en MySQL e interfaz HTML.
Las principales características son:

- Inclusión de fábricas (industria automotriz).
- Edición de Fábrica.
- Exclusión de Fábrica.

- Inclusión de modelos.
- Edición de plantillas.
- Exclución de plantillas.

- Inclusión de vehículos.
- Edición de Vehículos.
- Exclusión de Vehículos.

- Sistema de filtro en cada pantalla.
 
 
 Instrucciones de cómo iniciar la aplicación
 
 1. Extraer el archivo Techo.rar en su directorio htdocs o hacer check-out del proyecto a través del git.
 2. El proyecto puede ser accedido a través del directorio http: // localhost / techo
 3. Ejecute los scripts de banco que se encuentran en la carpeta "htdocs \ techo \ scripts", si se ejecutan en la secuencia numérica no tendrá problemas.
 4. Hasta el 4 Script son creaciones de tablas, los demás son inserciones de datos en las tablas.
 