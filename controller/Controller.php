<?php

//ini_set('display_errors',1);
//ini_set('display_startup_erros',1);
//error_reporting(E_ALL);

  header('Content-Type: application/json; charset=UTF-8',true);

	$cAcao    = $_POST['acao'];

  switch ($cAcao)
  {
    case 'GravarFabrica' : GravarFabrica($_POST);
    break;  
    
    case 'GravarModelo' : GravarModelo($_POST);
    break;
    
    case 'BuscaModelo' : BuscaModelo($_POST);
    break;
    
    case 'GravarVehiculo' : GravarVehiculo($_POST);
    break;
    
    case 'ExcluirFabrica' : ExcluirFabrica($_POST);
    break;
    
    case 'ExcluirModelo' : ExcluirModelo($_POST);
    break;
    
    case 'ExcluirVeiculo' : ExcluirVeiculo($_POST);
    break;
    
    case 'AtualizaFabrica' : AtualizaFabrica($_POST);
    break;
    
    case 'AtualizaModelo' : AtualizaModelo($_POST);
    break;
    
    case 'AtualizaVehiculo' : AtualizaVehiculo($_POST);
    break;
  }

  function GravarFabrica($aDados)
  {
    require_once '../model/Model.php';
    $oBj = new Model();
    
    $aDados['nombre'] = filter_var($aDados['nombre'], FILTER_SANITIZE_STRING);
    $bGravou = $oBj->GravarFabrica($aDados);
    try 
    {
        if($bGravou)
        {
            echo json_encode(array("results" => true));
        }
    }catch (\Exception $e) 
    {
        echo json_encode(array("results" => false));
    }
  }
  
  function GravarModelo($aDados)
  {
      require_once '../model/Model.php';
      $oBj = new Model();
      
      $aDados['nombre'] = filter_var($aDados['nombre'], FILTER_SANITIZE_STRING);
      $bGravou = $oBj->GravarModelo($aDados);
      try
      {
          if($bGravou)
          {
              echo json_encode(array("results" => true));
          }
      }catch (\Exception $e)
      {
          echo json_encode(array("results" => false));
      }
  }
  
  function BuscaModelo($aDados)
  {
      require_once '../model/Model.php';
      $oBj = new Model();
            
      $aRetorno = $oBj->BuscaModelo($aDados);
      try
      {
          if($aRetorno)
          {
              //ComboBox de Modelos
              $comboBoxModelo = '';
              $comboBoxModelo .= '<select name="modelo" id="modelo" class="form-control" >';
              $comboBoxModelo .= '<option value="0">-- SELECIONE --</option>';
              foreach ($aRetorno as $k => $v)
              {
                  $comboBoxModelo .= '<option value="'.$v['modelo_id'] .'">'. $v['modelo_nombre'] .'</option>';
              }
              $comboBoxModelo .= '</select>';
              
              echo json_encode(array("results" => $comboBoxModelo));
          }
      }catch (\Exception $e)
      {
          echo json_encode(array("results" => false));
      }
  }
  
  function GravarVehiculo($aDados)
  {
      require_once '../model/Model.php';
      $oBj = new Model();
      
      $bGravou = $oBj->GravarVehiculo($aDados);
      try
      {
          if($bGravou)
          {
              echo json_encode(array("results" => true));
          }
      }catch (\Exception $e)
      {
          echo json_encode(array("results" => false));
      }
  }
  
  function ExcluirFabrica($aPost)
  {
      $id = $aPost['id'];
      
      require_once '../model/Model.php';
      $oBj = new Model();
      $bExcluiu = $oBj->ExcluirFabrica($id);
      
      if($bExcluiu)
      {
          echo json_encode(array("results" => true));
      }
      else
      {
          echo json_encode(array("results" => false));
      }
  }
  
  function ExcluirModelo($aPost)
  {
      $id = $aPost['id'];
      
      require_once '../model/Model.php';
      $oBj = new Model();
      $bExcluiu = $oBj->ExcluirModelo($id);
      
      if($bExcluiu)
      {
          echo json_encode(array("results" => true));
      }
      else
      {
          echo json_encode(array("results" => false));
      }
  }
  
  function ExcluirVeiculo($aPost)
  {
      $id = $aPost['id'];
      
      require_once '../model/Model.php';
      $oBj = new Model();
      $bExcluiu = $oBj->ExcluirVeiculo($id);
      
      if($bExcluiu)
      {
          echo json_encode(array("results" => true));
      }
      else
      {
          echo json_encode(array("results" => false));
      }
  }
  
  function AtualizaFabrica($aPost)
  {
      require_once '../model/Model.php';
      $oBj = new Model();
      $bGravou = $oBj->AtualizaFabrica($aPost);
      
      if($bGravou)
      {
          echo json_encode(array("results" => true));
      }
      else
      {
          echo json_encode(array("results" => false));
      }
  }
  
  function AtualizaModelo($aPost)
  {
      require_once '../model/Model.php';
      $oBj = new Model();
      $bGravou = $oBj->AtualizaModelo($aPost);
      
      if($bGravou)
      {
          echo json_encode(array("results" => true));
      }
      else
      {
          echo json_encode(array("results" => false));
      }
  }
  
  function AtualizaVehiculo($aPost)
  {
      require_once '../model/Model.php';
      $oBj = new Model();
      $bGravou = $oBj->AtualizaVehiculo($aPost);
      
      if($bGravou)
      {
          echo json_encode(array("results" => true));
      }
      else
      {
          echo json_encode(array("results" => false));
      }
  }
  
?>   
 