
USE techo;

CREATE TABLE modelo (
   modelo_id INT(10) NOT NULL AUTO_INCREMENT,
   modelo_nombre VARCHAR(100) NOT NULL COLLATE utf8_general_ci COMMENT 'Nome do Modelo',
   id_fabrica INT(10) NOT NULL COMMENT 'ID da Fabrica',
  PRIMARY KEY (modelo_id),
   FOREIGN KEY (id_fabrica) REFERENCES fabrica(fabrica_id)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Tabela do Modelo';