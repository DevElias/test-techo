
USE techo;

CREATE TABLE veiculo (
   veiculo_id INT(10) NOT NULL AUTO_INCREMENT,
   veiculo_color VARCHAR(100) NOT NULL COLLATE utf8_general_ci COMMENT 'Cor do Veiculo',
   veiculo_motor VARCHAR(100) NOT NULL COLLATE utf8_general_ci COMMENT 'Motor do Veiculo',
   veiculo_kilometraje VARCHAR(100) NOT NULL COLLATE utf8_general_ci COMMENT 'Kilometraje do Veiculo',
   id_fabrica INT(10) NOT NULL COMMENT 'ID do Fabrica',
   id_modelo INT(10) NOT NULL COMMENT 'ID do Modelo',
  PRIMARY KEY (veiculo_id),
  FOREIGN KEY (id_fabrica) REFERENCES fabrica(fabrica_id),
  FOREIGN KEY (id_modelo) REFERENCES modelo(modelo_id)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Tabela de Veiculos';