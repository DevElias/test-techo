
USE techo;

INSERT INTO fabrica (fabrica_id, fabrica_nombre) VALUES ('1', 'Ford'); 
INSERT INTO fabrica (fabrica_id, fabrica_nombre) VALUES ('2', 'Fiat'); 
INSERT INTO fabrica (fabrica_id, fabrica_nombre) VALUES ('3', 'Hyundai'); 
INSERT INTO fabrica (fabrica_id, fabrica_nombre) VALUES ('4' , 'Honda'); 


INSERT INTO modelo (modelo_id, modelo_nombre, id_fabrica) VALUES ('1' , 'Focus', '1'); 
INSERT INTO modelo (modelo_id, modelo_nombre, id_fabrica) VALUES ('2' , 'Palio', '2');
INSERT INTO modelo (modelo_id, modelo_nombre, id_fabrica) VALUES ('3' , 'Uno Mille', '2');
INSERT INTO modelo (modelo_id, modelo_nombre, id_fabrica) VALUES ('4' , 'ix35', '3');
INSERT INTO modelo (modelo_id, modelo_nombre, id_fabrica) VALUES ('5' , 'i30', '3');
INSERT INTO modelo (modelo_id, modelo_nombre, id_fabrica) VALUES ('6' , 'HB20', '3');
INSERT INTO modelo (modelo_id, modelo_nombre, id_fabrica) VALUES ('7' , 'Civic', '4');
INSERT INTO modelo (modelo_id, modelo_nombre, id_fabrica) VALUES ('8' , 'Honda Fit', '4');

INSERT INTO veiculo (veiculo_id, veiculo_color, veiculo_motor, veiculo_kilometraje, id_fabrica, id_modelo) VALUES ('1' , 'Prata', '2.0', '0', '4', '7');
INSERT INTO veiculo (veiculo_id, veiculo_color, veiculo_motor, veiculo_kilometraje, id_fabrica, id_modelo) VALUES ('2' , 'Preto', '1.0', '1500', '2', '3');
INSERT INTO veiculo (veiculo_id, veiculo_color, veiculo_motor, veiculo_kilometraje, id_fabrica, id_modelo) VALUES ('3' , 'Prata', '1.8', '1800', '1', '1');
INSERT INTO veiculo (veiculo_id, veiculo_color, veiculo_motor, veiculo_kilometraje, id_fabrica, id_modelo) VALUES ('4' , 'Azul', '2.0', '1800', '3', '5');
