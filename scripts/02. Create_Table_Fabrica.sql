
USE techo;

CREATE TABLE fabrica (
   fabrica_id INT(10) NOT NULL AUTO_INCREMENT,
   fabrica_nombre VARCHAR(100) NOT NULL COLLATE utf8_general_ci COMMENT 'Nome da Empresa',
  PRIMARY KEY (fabrica_id)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Tabela de Empresas';